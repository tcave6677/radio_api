/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/***** Includes *****/
/* Standard C Libraries */
#include <stdlib.h>

/* Timer Libraries */
#include <time.h>
#include <signal.h>
#include <unistd.h>

/* TI Drivers */
#include <ti/drivers/rf/RF.h>
#include <ti/drivers/PIN.h>

/* Driverlib Header files */
#include DeviceFamily_constructPath(driverlib/rf_prop_mailbox.h)

/* Board Header files */
#include "Board.h"

/* Application Header files */ 
#include "radio_api/radio_api.h"

/***** Defines *****/

/***** Prototypes *****/

/***** Variable declarations *****/
/* Pin driver handle */
static PIN_Handle ledPinHandle;
static PIN_State ledPinState;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config pinTable[] =
{
#if defined(Board_CC1350_LAUNCHXL)
 Board_DIO30_SWPWR | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
#endif
 Board_PIN_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
 Board_PIN_LED2 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
 PIN_TERMINATE
};

static timer_t timerid;

static char pingMsg[4] = "ping";
RadioApi_RadioPacket_t pingTxRadioPacket;

static char pongMsg[4] = "pong";
RadioApi_RadioPacket_t pongTxRadioPacket;

static uint8_t rxPacketPayload[RadioApi_MAX_PAYLOAD_LENGTH] = {0};
RadioApi_RadioPacket_t rxPacket;

/***** Private Function definitions *****/

/***** Function definitions *****/

void *mainThread(void *arg0)
{
    RadioApi_RadioPacket_t* pRxRadioPakcet = {0};
    RadioApi_Params_t radioApiParams;

    RadioApi_init();

    RadioApi_Params_init(&radioApiParams);

    /* Open LED pins */
    ledPinHandle = PIN_open(&ledPinState, pinTable);
    if (ledPinHandle == NULL)
    {
        while(1);
    }
    PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, 0);
    PIN_setOutputValue(ledPinHandle, Board_PIN_LED2, 1);

    if( RadioApi_open(&radioApiParams) != RadioApi_Status_Success)
    {
        /* Failed to allocate space for all data entries */
        PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, 1);
        PIN_setOutputValue(ledPinHandle, Board_PIN_LED2, 1);
        while(1);
    }

    RadioApi_on();

    /* Initialize packets */
    pingTxRadioPacket.len = 4;
    pingTxRadioPacket.pData = (uint8_t*) pingMsg;
    pongTxRadioPacket.len = 4;
    pongTxRadioPacket.pData = (uint8_t*) pongMsg;
    rxPacket.len = 0;
    rxPacket.pData = rxPacketPayload;


    while(1)
    {
        RadioApi_Status_t txStatus;
        static uint32_t cnt = 0;

        RadioApi_receive_bytes(&rxPacket);
        if(rxPacket.len != 0)
        {
            /* only respond to a ping */
            if(memcmp(pRxRadioPakcet->pData, pingMsg, 4) == 0)
            {
                RadioApi_send_bytes(pongTxRadioPacket);
            }
            else
            {
                /* we got a pong to our ping */
                PIN_setOutputValue(ledPinHandle, Board_PIN_LED0, !PIN_getOutputValue(Board_PIN_LED0));
            }
        }
        usleep(1000);

        /* Send ping every 1s */
        if(cnt > 1000)
        {
            txStatus = RadioApi_send_bytes(pingTxRadioPacket);
            if(txStatus == RadioApi_Status_Success)
            {
                PIN_setOutputValue(ledPinHandle, Board_PIN_LED1, !PIN_getOutputValue(Board_PIN_LED1));
            }
            cnt = 0;
        }
        cnt++;
    }
}
