/******************************************************************************

 @file RadioApi.c

 @brief Sub1G Over the Air Download Protocol Module

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2016 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/

/***** Includes *****/
#include <string.h>
#include <ti/drivers/rf/RF.h>
#include <ti/drivers/dpl/HwiP.h>
#include <ti/drivers/utils/List.h>
#include DeviceFamily_constructPath(driverlib/rf_prop_mailbox.h)

#include <radio_api/radio_api.h>
#include "radio_api/RFQueue.h"
#include "radio_api/smartrf_settings/smartrf_settings.h"

/***** Defines *****/
/* NOTE: Only two data entries supported at the moment */
#define NUM_DATA_ENTRIES       2
/* The Data Entries data field will contain:
 * 1 Header byte (RF_cmdPropRx.rxConf.bIncludeHdr = 0x1)
 * Max 30 payload bytes
 * 1 status byte (RF_cmdPropRx.rxConf.bAppendStatus = 0x1) */
#define NUM_APPENDED_BYTES     2

/* Log radio events in the callback */
//#define LOG_RADIO_EVENTS

/***** Prototypes *****/
static void radioCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e);

/***** Variable declarations *****/

/* Set Default parameters structure */
static const RadioApi_Params_t RadioApi_defaultParams = {
    .dummy       = 0,
};

static RadioApi_Params_t RadioApi_params;

static RF_Object rfObject;
static RF_Handle rfHandle;

/* Buffer which contains all Data Entries for receiving data.
 * Pragmas are needed to make sure this buffer is aligned to a 4 byte boundary
 * (requirement from the RF core)
 */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_ALIGN(rxDataEntryBuffer, 4)
static uint8_t
rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                  RadioApi_MAX_PAYLOAD_LENGTH,
                                                  NUM_APPENDED_BYTES)];
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment = 4
static uint8_t
rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                  RadioApi_MAX_PAYLOAD_LENGTH,
                                                  NUM_APPENDED_BYTES)];
#elif defined(__GNUC__)
static uint8_t
rxDataEntryBuffer[RF_QUEUE_DATA_ENTRY_BUFFER_SIZE(NUM_DATA_ENTRIES,
                                                  RadioApi_MAX_PAYLOAD_LENGTH,
                                                  NUM_APPENDED_BYTES)]
                                                  __attribute__((aligned(4)));
#else
#error This compiler is not supported
#endif //defined(__TI_COMPILER_VERSION__)


/* Receive Statistics */
static rfc_propRxOutput_t rxStatistics;

/* Receive dataQueue for RF Core to fill in data */
uint8_t packetLength;
uint8_t* packetDataPointer;
static dataQueue_t dataQueue;
static rfc_dataEntryGeneral_t* currentDataEntry;

typedef struct {
    List_Elem             _elem;       /* Pointer to next and previous elements. */
    RadioApi_RadioPacket_t radioPacket;
    bool                   allocated;
}RadioApi_RadioPacketListEntry_t;


RadioApi_RadioPacket_t txRadioPacket = {0};

/* Rx Command Handle */
static RF_CmdHandle rxCmdHndl;
bool radioOn = false;
/* Rx packet container pool */
static uint8_t radioApi_rxDataBufPool[RadioApi_RX_Q_LEN][RadioApi_MAX_PAYLOAD_LENGTH];
static RadioApi_RadioPacketListEntry_t radioApi_rxPacketPool[RadioApi_RX_Q_LEN];

static List_List RadioApi_rxPacketList;

#ifdef LOG_RADIO_EVENTS
static volatile RF_EventMask eventLog[32];
static volatile uint8_t evIndex = 0;
#endif // LOG_RADIO_EVENTS

/***** Function definitions *****/
static RadioApi_RadioPacketListEntry_t* RadioApi_packetAlloc(void);
static void RadioApi_packetDelloc(RadioApi_RadioPacketListEntry_t* radioPacket);


/** @brief  Function that initializes the Wsn Protocol Task and creates all TI-RTOS objects
 *
 */
void RadioApi_init(void)
{
    uint8_t i;

    /* init packet pool */
    for (i = 0; i < RadioApi_RX_Q_LEN; i++)
    {
        radioApi_rxPacketPool[i].radioPacket.len = 0;
        radioApi_rxPacketPool[i].allocated = false;
        /* set pointer to buffer */
        radioApi_rxPacketPool[i].radioPacket.pData = radioApi_rxDataBufPool[i];
    }
}

/** @brief  Function to initialize the RadioApi_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 *  Defaults values are:
 *     dummy     = 0;
 */
void RadioApi_Params_init(RadioApi_Params_t *params)
{
    *params = RadioApi_defaultParams;
}

/** @brief  Function to open the RadioApi module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 */
RadioApi_Status_t RadioApi_open(RadioApi_Params_t *params)
{
    RF_Params rfParams;
    RF_Params_init(&rfParams);

    // Populate default params if not provided
    if (params == NULL)
    {
        RadioApi_Params_init(&RadioApi_params);
        params = &RadioApi_params;
        memcpy(params, &RadioApi_params, sizeof(RadioApi_Params_t));
    }
    else
    {
        memcpy(&RadioApi_params, params, sizeof(RadioApi_Params_t));
    }

    if( RFQueue_defineQueue(&dataQueue,
                            rxDataEntryBuffer,
                            sizeof(rxDataEntryBuffer),
                            NUM_DATA_ENTRIES,
                            RadioApi_MAX_PAYLOAD_LENGTH + NUM_APPENDED_BYTES))
    {
        return RadioApi_Status_Failed;
    }

    /* Modify CMD_PROP_TX and CMD_PROP_RX commands for application needs */
    RF_cmdPropTx.pktLen = 0;
    RF_cmdPropTx.pPkt = NULL;
    RF_cmdPropTx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropTx.startTrigger.pastTrig = 1;
    RF_cmdPropTx.startTime = 0;
    RF_cmdPropTx.pNextOp = NULL;

    /* Set the Data Entity queue for received data */
    RF_cmdPropRx.pQueue = &dataQueue;
    /* Discard ignored packets from Rx queue */
    RF_cmdPropRx.rxConf.bAutoFlushIgnored = 1;
    /* Discard packets with CRC error from Rx queue */
    RF_cmdPropRx.rxConf.bAutoFlushCrcErr = 1;
    /* Implement packet length filtering to avoid PROP_ERROR_RXBUF */
    RF_cmdPropRx.maxPktLen = RadioApi_MAX_PAYLOAD_LENGTH;
    RF_cmdPropRx.pktConf.bRepeatOk = 1;
    RF_cmdPropRx.pktConf.bRepeatNok = 1;
    RF_cmdPropRx.startTrigger.triggerType = TRIG_NOW;
    RF_cmdPropRx.pOutput = (uint8_t *)&rxStatistics;


    /* Request access to the radio */
    rfHandle = RF_open(&rfObject, &RF_prop,
                       (RF_RadioSetup*)&RF_cmdPropRadioDivSetup, &rfParams);

    /* Set the frequency */
    RF_postCmd(rfHandle, (RF_Op*)&RF_cmdFs, RF_PriorityNormal, NULL, 0);

    if(rfHandle == NULL)
    {
        return RadioApi_Status_Failed;
    }

    /* Set the frequency */
    RF_postCmd(rfHandle, (RF_Op*)&RF_cmdFs, RF_PriorityNormal, NULL, 0);

    return RadioApi_Status_Success;
}

/** @brief  Turns the radio on. This needs to be explicitly called since the
 *          radio draws power and takes up memory that you may otherwise need.
 *
 *  @param  none
 */
RadioApi_Status_t RadioApi_on(void)
{
    RF_ScheduleCmdParams schParams;
    RF_ScheduleCmdParams_init(&schParams);

    radioOn = true;
    //todo: Check rxCmdHndl is not allocated

    rxCmdHndl = RF_scheduleCmd(rfHandle, (RF_Op*)&RF_cmdPropRx, &schParams,
              radioCallback, (RF_EventRxEntryDone |
              RF_EventLastCmdDone));

    return RadioApi_Status_Success;
}

/** @brief  Turns off the radio, thus saving power and memory.
 *
 *  @param  none
 */
RadioApi_Status_t RadioApi_off(void)
{
    radioOn = false;

    /* Cancel the Rx */
    RF_cancelCmd(rfHandle, rxCmdHndl, 0);

    return RadioApi_Status_Success;
}

/** @brief  Sends a message containing bytes.
 *
 *  @param  none
 *
 *  @return txRadioPacket Radio Packet on Queue.
 */
RadioApi_Status_t RadioApi_send_bytes(RadioApi_RadioPacket_t txRadioPacket)
{
    RadioApi_Status_t status = RadioApi_Status_Failed;

    //check if packet is too large
    if(txRadioPacket.len > RadioApi_MAX_PAYLOAD_LENGTH)
    {
        txRadioPacket.len = RadioApi_MAX_PAYLOAD_LENGTH;
        //todo: Set error status?
    }

    RF_cmdPropTx.pktLen = txRadioPacket.len;
    RF_cmdPropTx.pPkt = txRadioPacket.pData;

    if(radioOn)
    {
        /* Cancel the Rx */
        RF_cancelCmd(rfHandle, rxCmdHndl, 0);
    }

    /* Transmit a packet and wait for its echo.
     * - When the first of the two chained commands (TX) completes, the
     * RF_EventCmdDone event is raised but not RF_EventLastCmdDone
     * - The RF_EventLastCmdDone in addition to the RF_EventCmdDone events
     * are raised when the second, and therefore last, command (RX) in the
     * chain completes
     * -- If the RF core successfully receives the echo it will also raise
     * the RF_EventRxEntryDone event
     * -- If the RF core times out while waiting for the echo it does not
     * raise the RF_EventRxEntryDone event
     */
    RF_EventMask terminationReason =
            RF_runCmd(rfHandle, (RF_Op*)&RF_cmdPropTx, RF_PriorityNormal,
                      radioCallback, (RF_EventCmdDone | RF_EventRxEntryDone |
                      RF_EventLastCmdDone));

    switch(terminationReason)
    {
        case RF_EventLastCmdDone:
            //success
            status = RadioApi_Status_Success;
            break;
        case RF_EventCmdCancelled:
        case RF_EventCmdAborted:
        case RF_EventCmdStopped:
        default:
            status = RadioApi_Status_Failed;
    }

    uint32_t cmdStatus = ((volatile RF_Op*)&RF_cmdPropTx)->status;
    switch(cmdStatus)
    {
        case PROP_DONE_OK:
            // Packet transmitted successfully
            status = RadioApi_Status_Success;
            break;
        case PROP_DONE_STOPPED:
            // received CMD_STOP while transmitting packet and finished
            // transmitting packet
        case PROP_DONE_ABORT:
            // Received CMD_ABORT while transmitting packet
        case PROP_ERROR_PAR:
            // Observed illegal parameter
        case PROP_ERROR_NO_SETUP:
            // Command sent without setting up the radio in a supported
            // mode using CMD_PROP_RADIO_SETUP or CMD_RADIO_SETUP
        case PROP_ERROR_NO_FS:
            // Command sent without the synthesizer being programmed
        case PROP_ERROR_TXUNF:
            // TX underflow observed during operation
        default:
            // Uncaught error event - these could come from the
            // pool of states defined in rf_mailbox.h
            status = RadioApi_Status_Failed;
    }

    if(radioOn)
    {
        /* Turn radio back on */
        RadioApi_on();
    }
    return status;
}

/** @brief  Receive the next incoming message on the message queue. Returns
 *          None if there are no pending messages. Messages are returned as
 *          bytes.
 *
 *  @param  pRxPacket buffer to copy packet in to
 *
 *  @return none
 */
void RadioApi_receive_bytes(RadioApi_RadioPacket_t* pRxPacket)
{
    RadioApi_RadioPacketListEntry_t* rxPacketEntry;

    /* Enter critical section */
    uint32_t key = HwiP_disable();

    rxPacketEntry = (RadioApi_RadioPacketListEntry_t*) List_head(&RadioApi_rxPacketList);

    if (rxPacketEntry)
    {
        memcpy(pRxPacket->pData, rxPacketEntry->radioPacket.pData, rxPacketEntry->radioPacket.len);
        pRxPacket->len = rxPacketEntry->radioPacket.len;

        pRxPacket = &(rxPacketEntry->radioPacket);
        List_remove(&RadioApi_rxPacketList, (List_Elem*)rxPacketEntry);
        RadioApi_packetDelloc(rxPacketEntry);
    }
    else
    {
        pRxPacket->len = 0;
    }

    /* Exit critical section */
    HwiP_restore(key);

    return pRxPacket;
}

/** @brief  Radio command Callback.
 *
 *  @param  h   Rf Client Handle
 *  @param  ch  Rf Command Handle
 *  @param  e   Event mask
 *
 *  @return None.
 */
static void radioCallback(RF_Handle h, RF_CmdHandle ch, RF_EventMask e)
{
#ifdef LOG_RADIO_EVENTS
    eventLog[evIndex++ & 0x1F] = e;
#endif// LOG_RADIO_EVENTS

    if (e & RF_EventRxEntryDone)
    {
        /* Successful RX - save the data */

        RadioApi_RadioPacketListEntry_t* rxPacketEntry = RadioApi_packetAlloc();

        if(rxPacketEntry != NULL)
        {
            /* Get current unhandled data entry */
            currentDataEntry = RFQueue_getDataEntry();

            /* Handle the packet data, located at &currentDataEntry->data:
             * - Length is the first byte with the current configuration
             * - Data starts from the second byte */
            packetLength      = *(uint8_t *)(&(currentDataEntry->data));
            packetDataPointer = (uint8_t *)(&(currentDataEntry->data) + 1);

            if(packetLength > RadioApi_MAX_PAYLOAD_LENGTH)
            {
                packetLength = RadioApi_MAX_PAYLOAD_LENGTH;
            }

            /* Copy the payload + status byte to the rxPacket variable, and then
             * over to the txPacket
             */
            memcpy(rxPacketEntry->radioPacket.pData, packetDataPointer, packetLength);
            rxPacketEntry->radioPacket.len = packetLength;

            /* Push packet on the List */
            List_put(&RadioApi_rxPacketList, (List_Elem*)rxPacketEntry);

            RFQueue_nextEntry();
        }

    }
    else if (e & RF_EventLastCmdDone)
    {
        /* Successful TX*/
    }
    else // any uncaught event
    {
        /* Error Condition */
    }
}

/** @brief  Allocate radio packet.
 *
 *
 *  @return allocated radio packet.
 */
static RadioApi_RadioPacketListEntry_t* RadioApi_packetAlloc(void)
{
    uint32_t i;
    for (i = 0; i < RadioApi_RX_Q_LEN; i++)
    {
        /* Find the first available entry in the command pool */
        if (!radioApi_rxPacketPool[i].allocated)
        {
            radioApi_rxPacketPool[i].allocated = true;
            return(&radioApi_rxPacketPool[i]);
        }
    }
    return(NULL);
}

/** @brief  Dellocate radio packet.
 *
 *
 *  @return allocated radio packet.
 */
static void RadioApi_packetDelloc(RadioApi_RadioPacketListEntry_t* radioPacket)
{
    if(radioPacket)
    {
        radioPacket->allocated = false;
    }
}

