/******************************************************************************

 @file radio_api.h

 @brief OAD Protocol Header

 Group: WCS LPC
 $Target Device: DEVICES $

 ******************************************************************************
 $License: BSD3 2016 $
 ******************************************************************************
 $Release Name: PACKAGE NAME $
 $Release Date: PACKAGE RELEASE DATE $
 *****************************************************************************/
/*!*****************************************************************************
 *  @file       radio_api.h
 *
 *  @brief      Wireless Sensor Network Protocol
 *
 *  The RadioApi interface provides device independent APIs, data types,
 *  and macros.
 *
 *  # Overview #
 *
 *  The Over the Air Download Protocol module provides protocol
 *  functionality on top of the RF protocol. 
 *
 *  The APIs in this module serve as an interface to a TIRTOS
 *  application and offers functionality for a OAD messaging protocol between
 *  OAD Sever and the OAD Client. The module  handles formating / parsing of
 *  messages.
 *
 *  # Usage #
 *
 *  To use the RadioApi module to format/parse OAD messages, the application
 *  calls the following APIs:
 *    - RadioApi_init(): Initialize the RadioApi module/task.
 *    - RadioApi_Params_init():  Initialize a RadioApi_Params structure
 *      with default values.  Then change the parameters from non-default
 *      values as needed.
 *    - RadioApi_open():  Open an instance of the RadioApi module,
 *      passing the initialized parameters.
 *    - RadioApi_sendFwRequest():  This is an example of an OAD message that
 *      is formated and sent.
 *
 *  The following code example opens RadioApi, sends a FW version request
 *  and processes the response.
 *
 *  @code
 *
 *  RadioApi_packetCBs_t RadioApiCbs = {
 *    NULL,              //Incoming FW Req
 *    fwVersionReqCb,    //Incoming FW Version Rsp
 *    NULL,              //Incoming Image Identify Req
 *    NULL,              //Incoming Image Identify Rsp
 *    NULL,              //Incoming OAD Block Req
 *    NULL,              //Incoming OAD Block Rsp
 *  };
 *
 * static void fwVersionRspCb(void* pSrcAddr, char *fwVersionStr)
 * {
 *   //Do something with srcAddr and fwVersionStr
 * }
 *
 * void someTaskInit(void)
 * {
 *   RadioApi_init();
 * }
 *
 * void someTaskFxn(void)
 * {
 *   // Set Default parameters structure
 *   static RadioApi_Params_t RadioApi_params;
 *
 *   // Initialize and open the Wsn Protocol Task
 *   RadioApi_Params_init(&RadioApi_params);
 *   RadioApi_params.pCallbacks = &RadioApiCbs;
 *   RadioApi_open(&RadioApi_params);
 *
 *   RadioApi_sendFwVersionReq(nodeAddress);
 *
 *  }
 *  @endcode
 *
 *
 *  ## RadioApi Configuration ##
 *
 *  In order to use the RadioApi APIs, the application is required
 *  to provide application specific configuration and callbacks.
 *
 *  @code
 *  RadioApi_Params_t RadioApi_Params = {
 *    pCallbacks;          // Application Callbacks for pressing packets
 *  };
 *  @endcode
 *
 *
 *  ## Initializing the RadioApi Module ##
 *
 *  RadioApi_init() must be called before any other RadioApi APIs.
 *  This function
 *  iterates through the elements of the SPI_config[] array, calling
 *  the element's device implementation SPI initialization function.
 *
 *
 *  ## Over the Air Download packets ##
 *
 *  The RadioApi supports OAD messages used to update the FW of a sensor
 *  node. The OAD is instigated by the concentrator sending
 *  RadioApi_PACKET_TYPE_OAD_IMG_IDENTIFY_REQ packet containing the image
 *  header. The sensor node checks the image header and sends a
 *  RadioApi_PACKET_TYPE_OAD_IMG_IDENTIFY_RSP containing the status, which
 *  is true f the age header is accepted or false if it is rejected. If the
 *  image header is accepted the sensor node requests the FW image blocks with
 *  the RadioApi_PACKET_TYPE_OAD_BLOCK_REQ and the concentrator sends the
 *  image blocks with RadioApi_PACKET_TYPE_OAD_BLOCK_RSP.
 *
 *  The message flow is:
 *
 *  Server                           Client
 *
 * (Optional FW version Req)
 *   FW_VERSION_REQ ----------------------->
 *       <-------------------------- FW_VERSION_RSP("rfWsnNode v03.01.00")
 *
 *   OAD_IMG_IDENTIFY_REQ ----------------->
 *       <-------------------------- OAD_IMG_IDENTIFY_RSP(status=1)
 *       <-------------------------- OAD_BLOCK_REQ(block=0)
 *   OAD_BLOCK_RSP(Block 0) --------------->
 *       <-------------------------- OAD_BLOCK_REQ(block=1)
 *   OAD_BLOCK_RSP(Block 1) --------------->
 *         ...
 *       <-------------------------- OAD_BLOCK_REQ(block=n)
 *   OAD_BLOCK_RSP(Block n) --------------->
 *
 *
 *******************************************************************************
 */
#ifndef RadioApi_H_
#define RadioApi_H_

#include "stdint.h"

/**
 *  @defgroup RadioApi_PACKET packet defines
 *  @{
 */
#define RadioApi_RATE_250KBIT   0 ///Constant used to indicate a throughput of 256 Kbit a second.
#define RadioApi_RATE_1MBIT     1 ///Constant used to indicate a throughput of 1 Mbit a second.
#define RadioApi_RATE_2MBIT     2 ///Constant used to indicate a throughput of 2 Mbit a second.

#define RadioApi_MAX_PAYLOAD_LENGTH     128
#define RadioApi_RX_Q_LEN               10

/** @}*/

/// RadioApi status codes
typedef enum {
    RadioApi_Status_Success, ///< Success
    RadioApi_Status_Failed, ///< Fail
} RadioApi_Status_t;

/** @brief RF parameter struct
 *  RF parameters are used with the RadioApi_open() and RadioApi_Params_init() call.
 */
typedef struct {
    uint32_t dummy;
} RadioApi_Params_t;

/** @brief RadioApi radio access functions
 *
 */
typedef struct
{
    uint32_t    len;   ///< length of packet
    uint8_t*    pData; ///< pointer to data buffer
} RadioApi_RadioPacket_t;

/** @brief  Function to initialize the RadioApi_Params struct to its defaults
 *
 *  @param  params      An pointer to RF_Params structure for
 *                      initialization
 *
 *  Defaults values are:
 *     dummy     = 0;
 */
extern void RadioApi_Params_init(RadioApi_Params_t *params);

/** @brief  Function that initializes the Wsn Protocol Task and creates all TI-RTOS objects
 *
 */
extern void RadioApi_init(void);

/** @brief  Function to open the RadioApi module
 *
 *  @param  params      An pointer to RF_Params structure for initialization
 */
extern RadioApi_Status_t RadioApi_open(RadioApi_Params_t *params);

/** @brief  Turns the radio on. This needs to be explicitly called since the
 *          radio draws power and takes up memory that you may otherwise need.
 *
 *  @param  none
 */
extern RadioApi_Status_t RadioApi_on(void);

/** @brief  Turns off the radio, thus saving power and memory.
 *
 *  @param  none
 */
extern RadioApi_Status_t RadioApi_off(void);

/** @brief  Sends a message containing bytes.
 *
 *  @param  none
 *
 *  @return radioPacket Radio Packet on Queue.
 */
extern RadioApi_Status_t RadioApi_send_bytes(RadioApi_RadioPacket_t radioPacket);

/** @brief  Receive the next incoming message on the message queue. Returns
 *          None if there are no pending messages. Messages are returned as
 *          bytes.
 *
 *  @param  pRxPacket buffer to copy packet in to
 *
 *  @return none
 */
extern void RadioApi_receive_bytes(RadioApi_RadioPacket_t* pRxPacket);

#endif /* RadioApi_H_ */
